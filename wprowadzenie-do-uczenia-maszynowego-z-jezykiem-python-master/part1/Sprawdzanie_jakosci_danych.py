#Created by Mariusz Liksza

# Sprawdzenie jakosci danych
# head, tail, describe, info <- methods
# shape, columns <- attributes

import pandas as pd

# Przed wczytaniem danych, jezeli masz taka mozliwosc, zapoznaj sie
# z dokumentacja dotyczaca danych, skad one sa, jakie informacje mozna w nich znalezc,
# jakie powinny byc zakresy i typy zmiennych
# Przykladowo, w przypadku danych HR, zajrzyj tu: https://www.kaggle.com/rhuebner/human-resources-data-set/home

# Wczytaj dane HR_dirty_data.txt
filename = 'HR_dirty_data.txt'
data_location = 'dane/'
HR_dirty_data = pd.read_csv(data_location + filename, sep=';', na_values=['', ' '])

# 'Rzuc okiem' na dane, zobacz czy cos Ci sie rzuca w oczy
HR_dirty_data.shape       # wyswietl liczbe obserwacji oraz liczbe zmiennych
HR_dirty_data.columns     # wyswietl nazwy zmiennych
HR_dirty_data.head()      # wyswietl 5. pierwszych obserwacji
HR_dirty_data.tail()      # wyswietl 5. ostatnich obserwacji
HR_dirty_data.describe()  # wyswietl podstawowe statystyki dla zmiennych numerycznych
HR_dirty_data.info()      # sprawdz ile jest brakow danych dla kazdej ze zmiennych oraz jakie sa typy zmiennych

HR_dirty_data.describe(include='all')  # Wyswietl więcej statystyk, dzieki czemu 'zalapia' się tez zmienne tekstowe

# Policz ile brakow wartosci ma każda z kolumn
HR_dirty_data.isnull().sum()

# Duzo wiecej informacji - pakiet pandas_profiling
import pandas_profiling

profile = pandas_profiling.ProfileReport(HR_dirty_data)
profile.to_file(outputfile="output.html")
