import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

# Ustaw Seaborn jako domyslny styl dla wszystkich wykresów
sns.set()

# Wczytaj dane HR_dirty_data.txt
filename = 'HR_dirty_data.txt'
data_location = 'dane/'
HR_dirty_data = pd.read_csv(data_location + filename, sep=';')

## Oczysc zmienne numeryczne ##
HR_dirty_data.describe()  # wyswietl podstawowe statystyki dla zmiennych numerycznych
# czyszczenie danych
# last evaluation powinno być w zakresie 0-1, describe() pokazało,
# że maksymalna wartosc wynosi 1.7
# sprawdz ile wartosci jest powyzej 1
sum(HR_dirty_data['last_evaluation'] > 1)  # 2

# Usunmy te wartosci (nie cale rekordy, tylko wartosci dla tej jednej kolumny)
mask = HR_dirty_data['last_evaluation'] > 1
HR_dirty_data.loc[mask, 'last_evaluation'] = np.nan

sum(HR_dirty_data['last_evaluation'] > 1)  # 0

# Sprawdzmy czy pozostale zmienne wygladaja ok
HR_dirty_data.describe()
# Niestety nie, podobna sytuacja w przypadku zmiennej number_project

# Sprawdzmy ile jest wartosci powyzej 100
sum(HR_dirty_data['number_project'] > 100)  # 1

# Usunmy tę jedna wartosc, analogicznie jak w przypadku last_evaluation
mask = HR_dirty_data['number_project'] > 100
HR_dirty_data.loc[mask, 'number_project'] = np.nan

sum(HR_dirty_data['number_project'] > 100)  # 0
HR_dirty_data.describe()

# Teraz maksymalna wartosc to 30, sprawdzmy histogram...
plt.hist(HR_dirty_data['number_project'].dropna())
plt.ylabel('Number of projects')
plt.show()

# Usunmy na chwile wartosc <= 8
HR_dirty_data_tmp = HR_dirty_data.loc[HR_dirty_data['number_project'] > 8, :]
plt.hist(HR_dirty_data_tmp['number_project'].dropna())
plt.show()

# ok, a wiec mamy jedna wartosc = 30, mozemy przyjac, ze zostala ona wprowadzona
# blednie, usunmy te wartosc
mask = HR_dirty_data['number_project'] == 30
HR_dirty_data.loc[mask, 'number_project'] = np.nan

HR_dirty_data.describe()

# W koncu udalo nam sie oczyscic zmienna number_project :)

# Sprawdzmy rozklad zmiennej time_spend_company (widzimy maksymalna podejrzana wartosc 10)
plt.hist(HR_dirty_data['time_spend_company'].dropna())
plt.show()

# Wyglada na to, ze jednak jest wiecej osob, ktore pracuja 10 lat :) Zostawiamy.

HR_dirty_data.describe()
# Zmienne Work_accident, left i promotion_last_5years maja wartosci -1,
# ktore sa niedopusczalne, poniewaz sa to zmienne logiczne o wartosciach 0 lub 1.
# Byc moze ktos wprowadzil je przez przypadek zamiast 0, jednakze nie mozemy tego zalozyc
# bez porozumienia sie z wlascicielem danych. W zwiazku z tym, usunmy wszystkie wartosci -1 z tych zmiennych
mask = HR_dirty_data['Work_accident'] < 0
HR_dirty_data.loc[mask, 'Work_accident'] = np.nan

mask = HR_dirty_data['left'] < 0
HR_dirty_data.loc[mask, 'left'] = np.nan

mask = HR_dirty_data['promotion_last_5years'] < 0
HR_dirty_data.loc[mask, 'promotion_last_5years'] = np.nan

# Ponadto zmienna Work_accident ma wartosc maksymalna = 2, co rowniez
# jest nie do przyjecia z tego samego powodu co wyzej, usunmy wartosci > 1
mask = HR_dirty_data['Work_accident'] > 1
HR_dirty_data.loc[mask, 'Work_accident'] = np.nan

HR_dirty_data.describe()

# lepiej :)

# Sprawdzmy teraz czy nie pominelismy jakis wartosci odstajacych przy pomocy box plot
# sprawdzamy tylko zmienne ciagle, tj. number_project, average_montly_hours, time_spend_company
plt.boxplot(HR_dirty_data['number_project'].dropna(), sym='k.')  # sym-'k.' dodany w celu zmiany ozaczenia dla wartosci
# odstajacych, jezeli modul seaborn jest zaimportowany to matplotlib nie pokazuje ich uzywajac domyslnego symbolu
plt.ylabel('Number of projects')
plt.show()

plt.boxplot(HR_dirty_data['average_montly_hours'].dropna(), sym='k.')
plt.ylabel('Average montly hours')
plt.show()

plt.boxplot(HR_dirty_data['time_spend_company'].dropna(), sym='k.')
plt.ylabel('Time spent in the company')
plt.show()

# box plot wskazał kilka wartości odstających dla zmiennej time_spend_company,
# jednakże nie będziemy ich usuwać tym razem

# Możemy rownież wykorzystać pakiec seaborn (https://seaborn.pydata.org/),
# aby uzyskać podobny efekt i troche mniej kodu :)
import seaborn as sns

sns.boxplot(HR_dirty_data['number_project'])
sns.boxplot(HR_dirty_data['average_montly_hours'])
sns.boxplot(HR_dirty_data['time_spend_company'])

## Teraz czas na zmienne tekstowe: sales i salary ##
# sales #
HR_dirty_data['sales'].unique()

# Po pierwsze usunmy wszystkie białe znaki
HR_dirty_data['sales'] = HR_dirty_data['sales'].str.replace(" ", "")

# Teraz sprawdzmy, ktore wartosci zostaly prawdopodobnie wprowadzone przez przypadek
# wystepuja tylko 1-2 razy
HR_dirty_data['sales'].value_counts()
# stworzmy mape, ktora zamieni nam te pojedyncze wartosci za ich poprawna wersje
# hrr -> hr, saless -> sales, Tech -> technical, tech -> technical, it -> IT
sales_map = {'hrr': 'hr', 'saless': 'sales', 'Tech': 'technical', 'tech': 'technical', 'it': 'IT'}
HR_dirty_data["sales"].replace(sales_map, inplace=True)
HR_dirty_data['sales'].value_counts()

# salary #
HR_dirty_data['salary'].unique()
HR_dirty_data['salary'] = HR_dirty_data['salary'].str.replace(" ", "")
HR_dirty_data['salary'].value_counts()
salary_map = {'mediu': 'medium', 'Medium': 'medium'}
HR_dirty_data['salary'].replace(salary_map, inplace=True)
HR_dirty_data['salary'].value_counts()

# uff... w końcu koniec :)
# albo i nie... :(
# teraz czas na braki wartosci

HR_dirty_data.info()  # sprawdz ile jest brakow danych dla kazdej ze zmiennych oraz jakie sa typy zmiennych
# Mamy ~15 tys. obserwacji, z czego każda ze zmiennych ma tylko kilka braków danych
# i to już po powyższym oczyszczeniu. W takiej sytuacji są trzy podstawowe opcje:
# 1. Usuwamy obserwacje z brakami danych
# 2. Uzupełniamy braki danych (jest wiele metod)
# 3. Zostawiamy tak jak jest, większość modeli radzi sobie z brakami danych.

# Moglibysmy zostawic to tak jak jest, ale dla celow treningowych zastosujmy metode 1 i 2
# do dwoch wybranych zmiennych

# Meotda 1 - Usuwamy obserwacje z brakami danych
HR_dirty_data_no_nan = HR_dirty_data.dropna()
HR_dirty_data_no_nan.info()
HR_dirty_data_no_nan.isnull().sum()

# Metoda 2 - Uzupełniamy braki danych
# Jedna z metod jest uzupelnienie brakow danych zadanymi wartosciami,
# jednakze jest to metoda niepolecana w przypadku kiedy nie wiemy jaka wartosci powinna byc uzyta
# Popularna i prosta metoda jest zastosowanie sredniej, jednakze nie jest to dobra metoda
# w przypadku duzej wariancji (srednia arytmetyczna kwadratow odchylen) w zbiorze.
# W przypadku duzej wariancji w zbiorze zdecydowanie lepiej wybrac mediane

# W naszym zbiorze wariancje zmiennych nie są zbyt dużę, zastem mozemy wybrac zarowno srednia
# jak i mediane, zastapmy braki danych srednimi
# oczywiscie nic nie stoi na przeszkodzie, aby uzyc mediany
# Wczesniej jednak, braki wartosci dla zmiennych logicznych (o wartosciach 0/1)
# zastapimy czesciej wystepujaca wartoscia
# zmienne logiczne to: Work_accident, left, promotion_last_5years
columns = ['Work_accident', 'left', 'promotion_last_5years']
HR_dirty_data_no_nan2 = HR_dirty_data.copy()
HR_dirty_data_no_nan2[columns] = HR_dirty_data_no_nan2[columns].fillna(HR_dirty_data_no_nan2.mode().iloc[0])

HR_dirty_data.describe(include='all')
HR_dirty_data_no_nan2 = HR_dirty_data_no_nan2.fillna(HR_dirty_data_no_nan2.mean().round(2))
HR_dirty_data_no_nan2.isnull().sum()

# Zauwazmy, ze zmienne tekstowe wciaz maja braki danych
# w takim przypadku, jednym ze sposobow jest stworzenie nowej kategorii
# dla braku danych
HR_dirty_data_no_nan2["sales"].fillna('no_information', inplace=True)
HR_dirty_data_no_nan2["salary"].fillna('no_information', inplace=True)

HR_dirty_data_no_nan2.info()
HR_dirty_data_no_nan2.describe()

# Zapisz oczyszczone dane
HR_dirty_data_no_nan2.to_csv(data_location + 'HR_cleaned.txt', index=False, sep=';')
