#Created by Mariusz Liksza

import pandas as pd

# Glowne komponenty pandas: Series i DataFrames
# W prostych slowach Series to kolumna a DataFrame to wielowymiarowa tabela,
# zbudowana z kolekcji Series

# Series
s1 = pd.Series([1, 2, 3, 4, 5])
s1
s2 = pd.Series([4, 2, 5, 21, 3])
s2
type(s1)

# DataFrame
df = pd.DataFrame({'s1_column': s1, 's2_column': s2})
df
df.dtypes
df.index  # sprawdz index

# albo od razu w jednej linijce
df2 = pd.DataFrame({'s1_column': s1,
                    's2_column': s2})
df2
df2.dtypes

# Mozemy tez pominac definiowanie obiektu Series, zostanie
# to automatycznie zrobione
df3 = pd.DataFrame({'s1_column': s1,
                    's2_column': s2})
type(df3.s1_column)

# zamien domyslny indeks liczbowy na date
dates = pd.date_range('20190901', periods=5)
dates
df4 = pd.DataFrame({'s1_column': s1,
                    's2_column': s2},
                   index=dates)
df4.index
df4

# Dodaj wiersz do istniejacej ramki danych
df2 = df2.append({'s1_column': 20, 's2_column': 13},
                 ignore_index=True)
df2

# Dodaj kolumne (Series) do istniejacej ramki danych
# Metoda 1 - dodaj kolumne na koniec deklarujac nowa liste
s3 = [20, 33, 43, 29, 3]
df4['s3_column'] = s3
df4

# Metoda 2 - insert
df3.insert(loc=1, column="s3_column", value=s3, allow_duplicates=True)
df3

# Sortowanie wynikow wzgledem wybranej kolumny
df3.sort_values(by='s3_column', ascending=True)
df3.sort_values(by='s3_column', ascending=False)
