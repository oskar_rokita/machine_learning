#Created by Mariusz Liksza

import pandas as pd

# Wczytaj dane z pliku csv i przechowuj je w obiekcie df (pandas dataframe object)
filename = 'HR_comma_sep.csv'
data_location = 'dane/'
HR_data = pd.read_csv(data_location + filename)  # sep=',' be default

HR_data.columns
HR_data.info()
HR_data.head()

# Stworz nowa ramke danych z kolumnami satisfaction_level, number_project, average_montly_hours i left
HR_data_narrowed = HR_data[['satisfaction_level', 'number_project', 'average_montly_hours', 'left']]
HR_data_narrowed.head()

# Stworz nowa ramke danych z kolumnami satisfaction_level, number_project, average_montly_hours i left,
# ale tym razem wez tylko pierwsze 3 obserwacje
HR_data_narrowed = HR_data[['satisfaction_level', 'number_project', 'average_montly_hours', 'left']][0:3]
HR_data_narrowed
# Zwroc uwage, ze nie wyswietlona zostala obserwacja z indeksem 3 - tak funkcjonuje wybierane danych
# w oparciu o indeksy liczbowe.

# Stworz nowa ramke danych ze zmiennymi od last_evaluation do left przy pomocy metody loc
HR_data_narrowed = HR_data.loc[:, 'last_evaluation':'left']
HR_data_narrowed.head()

# Stworz nowa ramke danych ze wszystkimi zmiennymi zaczynając od lewej strony i konczac na zmiennej
# Work_accident przy pomocy metody loc
HR_data_narrowed = HR_data.loc[:, :'Work_accident']
HR_data_narrowed.head()

# Stworz nowa ramke danych ze zmiennymi salary i left oraz wyswietl tylko obserwacje, dla ktorych salary = low
rows = 'low'
columns = ['salary', 'left']
mask = HR_data['salary'] == rows
HR_data_narrowed = HR_data.loc[mask, columns]

HR_data.loc[HR_data['salary'] == 'low', ['salary', 'left']]

# Sprawdzmy ile osob posiadajacych niska pensje odeszlo z pracy
HR_data['left'].value_counts()  # sposrod ~15 tys obserwacji, 3571 to osoby, ktore odeszly z firmy
HR_data['salary'].value_counts()  # low = 7316,  medium = 6446,  high = 1237
sum(HR_data_narrowed['left'] == 1)  # 2172
# Az 2172 osoby (z 3571, ktore odeszly) mialy niskie zarobki

# Mozemy rowniez zawezac ramke danych w oparciu o indeksy (zamiast nazw zmiennych/wartosci)
# wykorzystujac metode iloc
HR_data_narrowed = HR_data.iloc[0:4, 3:5]
HR_data_narrowed

# Nalezy uwazac na liczbe nawiasow kwadratowych
HR_data_tmp = HR_data[['salary']]
type(HR_data_tmp)  # DataFrame

HR_data_tmp = HR_data['salary']
type(HR_data_tmp)  # Series

# DataFrame vs Series
# Ramka danych to dwuwymiarowa struktura danych z etykietami (zarowno dla wierszy jak i kolumn).
# Seria/szereg to pojedyncza kolumna pochodzaca z ramki danych.
# Zatem, ramka danych sklada sie z jednej, badz wielu serii.


# Troche wiecej filtrow
# Wyswietl tylko osoby, ktore zrealizowaly co najmniej 4 projekty i maja ocene powyzej 0.9
HR_data_tmp = HR_data[(HR_data['number_project'] >= 4) & (HR_data['last_evaluation'] > 0.9)]

# Wyswietl osoby, ktore maja wysokie zarobki albo pracuja powyzej 6 lat
HR_data[['salary', 'time_spend_company']][0:10][(HR_data['salary'] == 'high') | (HR_data['time_spend_company'] > 6)]

# Wyswietl kolumny, ktore maja jakies braki danych
HR_data.loc[:, HR_data.isnull().any()]

# Stworz nowa zmienna na podstawie istniejacych
HR_data_tmp = HR_data  # stworzmy nowa ramke danych, do ktorej chcemy dodac zmienna
HR_data_tmp['satisf_eval'] = HR_data_tmp['satisfaction_level'] + HR_data_tmp['last_evaluation']


HR_data_tmp  # nowa zmienna
HR_data  # ups! rowniez nowa zmienna :(

# Stworz nowa zmienna na podstawie istniejacych, tym razem uzyj metody copy()
HR_data = pd.read_csv(data_location + filename)  # sep=',' be default
HR_data_tmp = HR_data.copy()  # nowa ramka danych bedzie tzw. gleboka kopia danych,
# oznacza to, ze w przypadku zmiany HR_data_tmp, HR_data pozostaje nieruszone
HR_data_tmp['satisf_eval'] = HR_data_tmp['satisfaction_level'] + HR_data_tmp['last_evaluation']

HR_data_tmp  # nowa zmienna
HR_data  # brak nowej zmiennej

# Aby zmienic nazwe kolumny uzywamy metody rename
HR_data_tmp.rename(columns={'satisf_eval': 'satisfaction + evaluation'}, inplace=True)
HR_data_tmp.columns
