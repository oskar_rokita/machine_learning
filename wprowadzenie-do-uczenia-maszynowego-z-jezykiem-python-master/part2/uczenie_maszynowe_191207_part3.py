#Warsztaty z obszaru Data Science

#Temat: algorytmy uczenia maszynowego

#model wielomianowy
#korzystamy z funkcji PolynomialFeatures ze stopniem 2,
#aby wygenerować nowe cechy, które są iloczynem cech bazowych,
#np. [x1,x2,x3] -> [x1, x2, x3, x1^2, x1x2, x1x3, x2^2, x2x3, x3^2]

import sklearn.preprocessing
wielomian2_cechy = sklearn.preprocessing.PolynomialFeatures(degree=2, include_bias=False)
wielomian2_cechy.fit_transform(np.array([[2,3,5],[1,2,3]]))

#możemy sprawdzić potęgi poszczególnych zmiennych (patrzymy na kolumny)
wielomian2_cechy.powers_.T

#budujemy model wielomianowy przekształcając zbiór treningowy predyktorów X_ucz
#oraz zbiór testowy predyktorów X_test
wielomian2 = sklearn.preprocessing.PolynomialFeatures(degree=2, include_bias=False)
X2_ucz = wielomian2.fit_transform(X_ucz)
X2_test = wielomian2.fit_transform(X_test)

#teraz mamy 65 kolumn
X2_ucz.shape

#sprawdzenie działania modelu
params.append("Reg. wielomianowa")
res.append(fit_regression(X2_ucz, X2_test, y_ucz, y_test))
pd.DataFrame(res, index=params)

#uzyskaliśmy mniejsze błędy dopasowania i predykcji, ale znacząco wzrosła liczba parametrów modelu

#redukcja zmiennych modelu
#szukamy równowagi pomiędzy złożonością modelu a jego jakością

#wyboru zmiennych do modelu możemy dokonać korzystając z kryterium Schwarza (BIC - Bayesian Information Criterion)
#wybieramy taki model regresji, który minimalizuje
#BIC(MSE_p, p, n) = n*log(MSE_p) + p*log(n)
#MSE_p jest liczone dla modelu zbudowanego na podstawie p<=d zmiennych
#p*log(n) to kara za złożoność modelu

def BIC(mse, p, n):
    return n*np.log(mse) + p*np.log(n)

#liczba wszystkich możliwych przypadków do rozpatrzenia jest rzędu 2^d
#1. zaczynamy od modelu pustego. BIC wynosi +nieskończoność
#2. rozszerzamy model o zmienną, dla której BIC jest najmniejsza i
#   jednocześnie zmniejsza aktualną wartość BIC - jeśli takiej nie ma zwracamy aktualny model
#3. powtarzamy 2. aż do wyczerpania możliwości

def forward_selection(X, y):
    n, m = X.shape
    best_idx = []
    best_free = set(range(m))
    best_fit = np.inf
    res = []
    
    for i in range(0, m):
        cur_idx = -1
        cur_fit = np.inf
        for e in best_free:
            r = sklearn.linear_model.LinearRegression()
            test_idx = best_idx + [e]
            r.fit(X[:, test_idx], y)
            test_fit = BIC(sklearn.metrics.mean_squared_error(y, r.predict(X[:, test_idx])), i+2, n)
            if test_fit < cur_fit: cur_idx, cur_fit = e, test_fit
        if cur_fit > best_fit: break
        
        best_idx, best_fit = best_idx + [cur_idx], cur_fit
        best_free.discard(cur_idx)
        res.append((cur_idx, cur_fit))
    return res
	
#stosujemy algorytm wyboru zmiennych do zbioru przekształconego wielomianowo
#okazuje się, że wybrano 20 zmiennych
wybrane_df = pd.DataFrame(forward_selection(X2_ucz, y_ucz), columns=["zmienna", "BIC"])
wybrane_zmienne = wybrane_df["zmienna"].tolist()
wybrane_df["nazwa"] = [X.columns[w>=1].append(X.columns[w==2]).str.cat(sep="*") for w in wielomian2.powers_[wybrane_zmienne]]
wybrane_df

#dodajemy kolejny wiersz do ramki danych z oceną jakości modelu
params.append("Reg. wiel. zmienne wybrane")
res.append(fit_regression(X2_ucz[:, wybrane_zmienne], X2_test[:, wybrane_zmienne], y_ucz, y_test))
wyniki = pd.DataFrame(res, index=params)
wyniki

#i rysujemy wykres
wyniki.drop(["r_score"], axis=1).plot(style=["-", ":", "--", "-."], color="k")
plt.xticks(np.arange(len(wyniki.index.tolist())), wyniki.index.tolist())
plt.show()

