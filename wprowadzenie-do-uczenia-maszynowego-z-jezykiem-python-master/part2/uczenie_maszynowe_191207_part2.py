#Warsztaty z obszaru Data Science

#Temat: algorytmy uczenia maszynowego

#tworzymy model regresji liniowej
import sklearn.linear_model
mnk = sklearn.linear_model.LinearRegression()

mnk.fit(X,y)

mnk.intercept_

mnk.coef_

white_wine.describe()

x_nowy = X.mean().values.reshape(1,-1)
x_nowy
#dodajmy też małą wartość do x_nowy, np. 0.001

mnk.predict(x_nowy)

#ocena jakości modelu
#porównanie wartości dopasowanych, obliczonych za pomocą modelu z wartościami oryginalnymi
y_pred = mnk.predict(X)
y_pred[0:8]

y[0:8]

#współczynnik determinacji R2
mnk.score(X,y)

sklearn.metrics.r2_score(y, y_pred)

#lub inne miary błędów dopasowania
#MSE
sklearn.metrics.mean_squared_error(y, y_pred)

#MAE
sklearn.metrics.mean_absolute_error(y, y_pred)

#MedAE
sklearn.metrics.median_absolute_error(y, y_pred)

#zależy nam na dobrych zdolnościach predykcyjnych modelu
#ale uważamy też żeby nie przeuczyć modelu, 
#zatem dzielimy zbiór na próbę uczącą (80%) i testową (20%)
X_ucz, X_test, y_ucz, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.2, random_state=12345)
print(X_ucz.shape)
print(X_test.shape)
print(y_ucz.shape)
print(y_test.shape)

#stworzymy funkcję, która dopasowuje model regresji liniowej do danej próby
#oraz oblicza miary błędów dopasowania
def fit_regression(X_ucz, X_test, y_ucz, y_test):
    r = sklearn.linear_model.LinearRegression()
    r.fit(X_ucz, y_ucz)
    y_ucz_pred = r.predict(X_ucz)
    y_test_pred = r.predict(X_test)
    mse = sklearn.metrics.mean_squared_error
    mae = sklearn.metrics.mean_absolute_error
    return {
        "r_score": r.score(X_ucz, y_ucz),
        "MSE_u": mse(y_ucz, y_ucz_pred),
        "MSE_t": mse(y_test, y_test_pred),
        "MAE_u": mae(y_ucz, y_ucz_pred),
        "MAE_t": mae(y_test, y_test_pred)
    }

#przedstawiamy działanie powyższej funkcji oraz wyniki
params = ["Reg. liniowa"]
res = [fit_regression(X_ucz, X_test, y_ucz, y_test)]
pd.DataFrame(res, index=params)

