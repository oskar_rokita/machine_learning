#Warsztaty z obszaru Data Science

#Temat: algorytmy uczenia maszynowego

#ładowanie niezbędnych pakietów
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn

#przykładowy zbiór danych - pomiary fizykochemiczne wlasności portugalskich win typu Vinho Verde (białe i czerwone) 
wine = pd.read_csv("winequality-all.csv", comment="#")
wine.head()

wine.info()

#kolor wina jest typu object, więc musimy zmienić tą zmienną na zmienną kategoryczną
wine.color = wine.color.astype("category")
wine.info()

#przydatne informacje o zbiorze, które można zapisać w postaci zmiennych
wine.shape

print(wine.columns.str.cat(sep=", "))

wine.describe()

wine.iloc[:, 0:11].describe().round(1).T.iloc[:, 1:]

#-----------------
#cel - sprawdzimy czy alkohol jest funkcją pozostałych 10 zmiennych i jaka jest ta zależność
#dzięki temu będziemy w stanie wyjaśnić pochodną jakiego zbioru czynników jest dana zawartość alkoholu
#a także przewidzieć zawartość alkoholu w nowo wyprodukowanej partii wina

#sprawdzamy ile win czerwonych i ile białych jest w naszym zbiorze
wine.color.value_counts()

#zajmiemy się winami białymi, ponieważ jest ich więcej i są słabsze :)
white_wine = wine[wine.color == "white"]
white_wine = white_wine.iloc[:, 0:11]
white_wine.head()

#tworzymy macierze zmiennych objaśniających (predyktorów) i wektor kolumnowy zmiennej objaśnianej
y = white_wine.iloc[:, -1]
y.head()
X = white_wine.iloc[:, :-1]
X.head()

#obliczmy wspóczynnik korelacji liniowej Pearsona
corr_P = white_wine.corr("pearson")
corr_P.shape
corr_P

#tworzymy macierz trójkątną i wyświetlamy wspóczynnik korelacji większy od 0.5
corr_P_tri = corr_P.where(np.triu(np.ones(corr_P.shape, dtype=np.bool), k=1)).stack().sort_values()
corr_P_tri

corr_P_tri[abs(corr_P_tri)>0.5]

#wizualizacja skupień przy pomocy seaborn pairplot
sns.pairplot(white_wine)
plt.show()

