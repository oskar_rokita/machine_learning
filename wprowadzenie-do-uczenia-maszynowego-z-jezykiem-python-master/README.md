# Wprowadzenie do uczenia maszynowego z jezykiem Python

**Opis:**

Sztuczna inteligencja (AI), Big Data, uczenie maszynowe (ML) to zagadnienia pojawiające się niezwykle często w otaczającej nas przestrzeni medialnej. Jako ludzkość czerpiemy już korzyści wynikające z wykorzystywania w życiu codziennym metod sztucznej inteligencji. W ramach niniejszego szkolenia chciałbym wprowadzić uczestników do świata uczenia maszynowego. Tematyka związana z ML jest bardzo szeroka, zatem na szkoleniu zostaną zaprezentowane tylko wybrane, ale reprezentatywne obszary. Szkolenie zostanie przeprowadzone z wykorzystaniem języka Python, który staje się obecnie coraz bardziej popularny i jest stosowany właściwie we wszystkich branżach do realizacji różnorodnych celów.

**Prowadzący:**     dr inż. Patryk Jasik

**Data:** 				07.12.2019

**Miejsce:** 			sala 403GG

**Czas:** 				9:00 – 16:00

**Dla kogo:** 	osoby rozpoczynające swoją przygodę z uczeniem maszynowym (poziom początkujący)

**Warunek uczestnictwa:** 	podstawowa znajomość języka Python


**Agenda:**

*  (9:00 – 10:30)	Wstęp do pracy z danymi w języku Python

*  (10:45 – 12:15)	Preprocessing danych, przygotowanie danych i eksploracyjna analiza danych (EDA)

*  (12:45 – 14:15)	Modelowanie danych – uczenie maszynowe przy użyciu pakietu scikit-learn 

*  (14:30 – 16:00)	Ocena jakości modeli oraz poprawa jakości modeli
